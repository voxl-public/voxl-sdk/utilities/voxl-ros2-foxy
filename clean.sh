#!/bin/bash
#
# Modal AI Inc. 2023
# author: zachary.lowell@ascendengineer.com


rm -rf build/
rm -rf build32/
rm -rf build64/
rm -rf pkg/control.tar.gz
rm -rf pkg/data/
rm -rf pkg/data.tar.gz
rm -rf pkg/DEB/
rm -rf pkg/IPK/
rm -rf *.deb
rm -rf .bash_history

rm -rf misc_files
